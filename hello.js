var http = require('http');
var APP = {
	HOST: "0.0.0.0",
	PORT: 3000
};

http.createServer(function (req, res) {
  res.writeHead(200);
  res.write("<h1>My name is Antwaan</h1>");  
  res.end('Hello World\n');
}).listen(APP.PORT, APP.HOST);

console.log('Server running at http://'+APP.HOST+':'+APP.PORT);
