#CodeSmith Vagrant Setup

###Software Stack
- Node.js
- PostgreSQL
- Koa.io
- Babel
- Ansible

###Get Started
- Install NPM Packages
```
npm install
```

- Start Server
```
npm start
```
