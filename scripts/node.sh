#!/usr/bin/env bash

#update repos
echo '------------------ UPDATE ------------------'
sudo apt-get update

#Then use apt-get to install Node.js build dependencies:
sudo apt-get install -y build-essential openssl libssl-dev pkg-config

echo '------------------ INSTALL NODE ------------------'
#Setup with Ubuntu:
curl --silent --location https://deb.nodesource.com/setup_4.x | sudo bash -

#Then install with Ubuntu:
sudo apt-get install --yes nodejs

#Set Up Reverse Proxy Server
echo '------------------ INSTALL NGINX ------------------'
sudo apt-get install nginx -y
echo '------------------ CONFIGURE NGINX ------------------'
# sudo vim /etc/nginx/sites-available/default

# Delete everything in the file and insert the following configuration.
# Be sure to substitute your own domain name for the server_name
# directive (or IP address if you don't have a domain set up),
# and the app server private IP address for the APP_PRIVATE_IP_ADDRESS.
# Additionally, change the port (8080) if your application is set to
# listen on a different port:
sudo echo "
server {
    listen 80;

    server_name example.com;

    location / {
        proxy_pass http://APP_PRIVATE_IP_ADDRESS:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
" >> /etc/nginx/sites-available/default

#https://github.com/Unitech/pm2
# sudo npm install pm2 -g

#The startup subcommand generates and configures a startup script to launch PM2
# and its managed processes on server boots. You must also specify the platform
# you are running on, which is ubuntu, in our case:
# pm2 startup ubuntu

#    Output:
#    [PM2] You have to run this command as root
#    [PM2] Execute the following command :
#    [PM2] sudo env PATH=$PATH:/usr/local/bin pm2 startup ubuntu -u sammy

# Run the command that was generated (similar to the highlighted output above)
# to set PM2 up to start on boot (use the command from your own output):
# sudo su -c "env PATH=$PATH:/usr/bin pm2 startup ubuntu -u vagrant"
sudo apt-get update

